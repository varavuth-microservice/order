'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Order.init({
    user_id: DataTypes.INTEGER,
    products: DataTypes.JSON
  }, {
    sequelize,
    modelName: 'Order',
    tableName: 'orders',
    underscored: true,
    createdAt:'created_at',
    updatedAt:'updated_at'

  });
  return Order;
};