const express = require('express');
const router = express.Router();



const model = require('../models/index');
/* /api/v1/users/ */


router.get('/', async function(req, res, next) {  
  const orders = await model.Order.findAll();
 
  const totalOrders = await model.Order.count();

  return res.status(200).json({
    total:totalOrders,
    data: orders
  });
});

/* /api/v1/users/create */
router.post('/create', async function(req, res, next) {
  const {products} = req.body;

  

  //3. บันทึกลงตาราง users
  const newOrder = await model.Order.create({
    user_id: req.user.user_id,
    products: products
  });

  return res.status(201).json({
    Order: {
      id: newOrder.id,
      user_id: newOrder.user_id,
      products: newOrder.products
    },
    message: 'สร้างรายงานสินค้าสำเร็จ'
  });
});

module.exports = router;
